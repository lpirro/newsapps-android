package com.lpirro.fruit.app.application

import android.app.Application
import com.lpirro.fruit.app.fruitlist.FruitListModule
import com.lpirro.fruit.data.Repository
import com.lpirro.fruit.data.models.LogType
import com.lpirro.fruit.di.ApplicationComponent
import com.lpirro.fruit.di.ApplicationModule
import com.lpirro.fruit.di.DaggerApplicationComponent
import com.lpirro.fruit.utils.AppSchedulerProvider
import com.lpirro.fruit.utils.FruitLogger
import com.lpirro.fruit.utils.LoggerCallback

class FruitApplication: Application() {

    lateinit var component: ApplicationComponent
        private set


    private val logger: FruitLogger by lazy {
        FruitLogger(Repository(), AppSchedulerProvider())
    }

    override fun onCreate() {
        super.onCreate()

        component = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .fruitListModule(FruitListModule())
                .build()

        Thread.setDefaultUncaughtExceptionHandler { thread, e -> handleUncaughtException(thread, e) }
    }

    private fun handleUncaughtException(thread: Thread, e: Throwable) {
        logger.logEvent(LogType.ERROR, e.toString(), object : LoggerCallback {
            override fun onLogPerformed() {
                System.exit(0)
            }
        })
    }
}