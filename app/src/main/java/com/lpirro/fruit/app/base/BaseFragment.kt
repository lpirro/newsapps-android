package com.lpirro.fruit.app.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.lpirro.fruit.R

abstract class BaseFragment: Fragment(){

    @LayoutRes
    protected abstract fun layoutId(): Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(layoutId(), container, false)
    }

    fun showErrorDialog(title: String = resources.getString(R.string.app_name), message: String?){
        val builder = AlertDialog.Builder(context!!).apply {
            setTitle(title)
            setMessage(message)
            setPositiveButton(android.R.string.ok){ dialog, _ ->
                dialog.dismiss()
            }
        }

        val dialog: AlertDialog = builder.create()
        dialog.show()
    }

}