package com.lpirro.fruit.app.fruitdetail

import android.os.Bundle
import android.view.View
import com.lpirro.fruit.R
import com.lpirro.fruit.app.base.BaseFragment
import com.lpirro.fruit.data.models.Fruit
import kotlinx.android.synthetic.main.fragment_fruit_detail.*

class FruitDetailFragment: BaseFragment(){

    override fun layoutId() = R.layout.fragment_fruit_detail

    private lateinit var fruit: Fruit

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        fruit = FruitDetailFragmentArgs.fromBundle(arguments).fruit

        tvFruitName.text = fruit.type
        tvFruitPrice.text = getString(R.string.fruit_price, fruit.getFormattedPrice().toString())
        tvFruitWeight.text = getString(R.string.fruit_weight, fruit.getWeightInKg().toString())

    }
}