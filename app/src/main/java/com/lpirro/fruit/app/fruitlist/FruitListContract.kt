package com.lpirro.fruit.app.fruitlist

import com.lpirro.fruit.data.models.Fruit
import com.lpirro.fruit.mvp.View

interface FruitListContract {

    interface FruitsView: View {
        fun showFruits(fruits: List<Fruit>)
        fun showFruitsDetailUi(fruit: Fruit)
    }

    interface Presenter {
        fun getFruits()
        fun openFruitDetail(clickedFruit: Fruit)
    }
}