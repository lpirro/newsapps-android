package com.lpirro.fruit.app.fruitlist

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.lpirro.fruit.R
import com.lpirro.fruit.app.application.FruitApplication
import com.lpirro.fruit.app.base.BaseFragment
import com.lpirro.fruit.app.fruitlist.adapter.FruitListAdapter
import com.lpirro.fruit.app.fruitlist.interfaces.FruitClickListener
import com.lpirro.fruit.data.Repository
import com.lpirro.fruit.data.models.Fruit
import com.lpirro.fruit.data.models.LogType
import com.lpirro.fruit.utils.AppSchedulerProvider
import com.lpirro.fruit.utils.FruitLogger
import com.lpirro.fruit.utils.Timelog
import kotlinx.android.synthetic.main.fragment_fruit_list.*
import javax.inject.Inject

const val ARG_FRUITS = "arg_fruits"

class FruitListFragment : BaseFragment(), FruitListContract.FruitsView, FruitClickListener {

    override fun layoutId() = R.layout.fragment_fruit_list

    private val timeLog = Timelog()

    @Inject
    lateinit var presenter: FruitListPresenter
    @Inject
    lateinit var logger: FruitLogger

    private val adapter: FruitListAdapter by lazy {
        FruitListAdapter(this)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        (activity?.application as FruitApplication).component.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.attachView(this)

        if (savedInstanceState == null)
            loadData()
        else {
            val fruits = savedInstanceState.getParcelableArrayList<Fruit>(ARG_FRUITS)
            adapter.setData(fruits)
        }

        fruitRecyclerView.layoutManager = LinearLayoutManager(context!!)
        fruitRecyclerView.hasFixedSize()
        fruitRecyclerView.adapter = adapter

        swipeRefreshLayout.setOnRefreshListener {
            timeLog.resetStartTime()
            loadData()
        }
    }

    override fun onFruitClicked(fruit: Fruit) {
        presenter.openFruitDetail(fruit)
    }

    override fun showFruitsDetailUi(fruit: Fruit) {
        val fruitDetailDirections =
                FruitListFragmentDirections.actionFruitListFragmentToFruitDetailFragment(fruit)

        view?.findNavController()?.navigate(fruitDetailDirections)
    }

    override fun showFruits(fruits: List<Fruit>) {
        logger.logEvent(LogType.DISPLAY, timeLog.elapsedTime.toString())
        adapter.setData(fruits)
    }

    override fun showLoading(active: Boolean) {
        fruitRecyclerView.scheduleLayoutAnimation()
        swipeRefreshLayout.isRefreshing = active
    }

    override fun showError(error: Throwable) {
        showErrorDialog(message = error.localizedMessage)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        val arrayList = ArrayList(adapter.getData())
        outState.putParcelableArrayList(ARG_FRUITS, arrayList)
    }

    override fun onStop() {
        super.onStop()
        presenter.unsubscribe()
    }

    private fun loadData() {
        presenter.getFruits()
    }
}