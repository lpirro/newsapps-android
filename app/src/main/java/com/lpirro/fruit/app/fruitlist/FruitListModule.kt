package com.lpirro.fruit.app.fruitlist

import com.lpirro.fruit.data.Repository
import com.lpirro.fruit.utils.AppSchedulerProvider
import com.lpirro.fruit.utils.FruitLogger
import com.lpirro.fruit.utils.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class FruitListModule{

    @Provides
    @Singleton
    fun provideFruitPresenter (repository: Repository, schedulerProvider: SchedulerProvider): FruitListPresenter {
        return FruitListPresenter(repository, schedulerProvider)
    }

    @Provides
    @Singleton
    fun provideLogger (repository: Repository, schedulerProvider: SchedulerProvider): FruitLogger {
        return FruitLogger(repository, schedulerProvider)
    }
}