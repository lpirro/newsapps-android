package com.lpirro.fruit.app.fruitlist

import com.lpirro.fruit.data.Repository
import com.lpirro.fruit.data.models.Fruit
import com.lpirro.fruit.mvp.BasePresenter
import com.lpirro.fruit.utils.SchedulerProvider
import io.reactivex.disposables.CompositeDisposable

class FruitListPresenter(private val repository: Repository, private val schedulerProvider: SchedulerProvider) : BasePresenter<FruitListContract.FruitsView>(),
        FruitListContract.Presenter {

    private val mCompositeDisposable = CompositeDisposable()

    override fun unsubscribe() {
        mCompositeDisposable.clear()
    }

    override fun getFruits() {

        view?.showLoading(true)

        val disposable = repository.getFruits()
                .subscribeOn(schedulerProvider.ioScheduler())
                .observeOn(schedulerProvider.uiScheduler())
                .map { response -> response.fruit }
                .doFinally { view?.showLoading(false) }
                .subscribe(
                        { fruits -> view?.showFruits(fruits) },
                        { error -> view?.showError(error) }
                )

        mCompositeDisposable.add(disposable)
    }

    override fun openFruitDetail(clickedFruit: Fruit) {
        view?.showFruitsDetailUi(clickedFruit)
    }

}