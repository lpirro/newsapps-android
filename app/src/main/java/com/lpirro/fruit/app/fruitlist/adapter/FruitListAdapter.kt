package com.lpirro.fruit.app.fruitlist.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.lpirro.fruit.R
import com.lpirro.fruit.app.fruitlist.interfaces.FruitClickListener
import com.lpirro.fruit.data.models.Fruit
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_fruit.*
import java.util.*


class FruitListAdapter(private val listener: FruitClickListener? = null): RecyclerView.Adapter<FruitListAdapter.FruitViewHolder>() {

    private var data: List<Fruit> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FruitViewHolder {
        return FruitViewHolder(
                LayoutInflater.from(parent.context)
                        .inflate(R.layout.view_fruit, parent, false)
        )
    }

    override fun getItemCount() = data.size

    override fun onBindViewHolder(holder: FruitViewHolder, position: Int) {

        val currentFruit = data[position]

        holder.fruitName.text = currentFruit.type ?: "N/A"
        holder.itemView.setOnClickListener { listener?.onFruitClicked(currentFruit) }
    }

    fun setData(data: List<Fruit>) {
        this.data = data
        notifyDataSetChanged()
    }

    fun getData() = data

    inner class FruitViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer

}