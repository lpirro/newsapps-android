package com.lpirro.fruit.app.fruitlist.interfaces

import com.lpirro.fruit.data.models.Fruit

interface FruitClickListener {
    fun onFruitClicked(fruit: Fruit)
}