package com.lpirro.fruit.data

import com.lpirro.fruit.data.models.FruitApiResponse
import com.lpirro.fruit.data.models.LogType
import com.lpirro.fruit.data.network.api.ApiService
import com.lpirro.fruit.data.network.api.ApiServiceFactory
import io.reactivex.Maybe
import io.reactivex.Single

open class Repository(var apiService: ApiService = ApiServiceFactory.makeApiService()) {

    fun getFruits(): Single<FruitApiResponse> {
        return apiService.getFruit()
    }

    fun logEvent(logType: LogType, data: String): Maybe<Void> {
        return apiService.logEvent(logType.logName, data)
    }
}