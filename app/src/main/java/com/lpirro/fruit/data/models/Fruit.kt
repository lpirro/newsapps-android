package com.lpirro.fruit.data.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Fruit(val type: String?, val price: Double, val weight: Double): Parcelable {

    fun getWeightInKg() = weight / 1000
    fun getFormattedPrice() = price / 100
}