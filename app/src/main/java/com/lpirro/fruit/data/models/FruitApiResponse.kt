package com.lpirro.fruit.data.models

data class FruitApiResponse (val fruit: List<Fruit>)