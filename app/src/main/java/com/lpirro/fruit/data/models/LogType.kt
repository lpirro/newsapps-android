package com.lpirro.fruit.data.models

enum class LogType (val logName: String){
        LOAD("load"),
        DISPLAY("display"),
        ERROR("error")
    }