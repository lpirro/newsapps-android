package com.lpirro.fruit.data.network.api

import com.lpirro.fruit.BuildConfig
import com.lpirro.fruit.data.models.FruitApiResponse
import io.reactivex.Maybe
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("fmtvp/recruit-test-data/master/data.json")
    fun getFruit(): Single<FruitApiResponse>

    @GET(BuildConfig.LOG_PATH_URL)
    fun logEvent(@Query("event") eventQuery: String,
                     @Query("data") dataQuery: String): Maybe<Void>

}
