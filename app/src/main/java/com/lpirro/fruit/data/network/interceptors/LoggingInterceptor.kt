package com.lpirro.fruit.data.network.interceptors

import com.lpirro.fruit.BuildConfig
import com.lpirro.fruit.data.Repository
import com.lpirro.fruit.data.models.LogType
import com.lpirro.fruit.utils.AppSchedulerProvider
import com.lpirro.fruit.utils.FruitLogger
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import java.util.concurrent.TimeUnit

class LoggingInterceptor : Interceptor {
    @Throws(IOException::class  )
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val path = request.url().url().path.substring(1)
        val startNs = System.nanoTime()

        val result: Response?

        try {
            result = chain.proceed(request)
        } catch (e: Exception) {
            throw e
        }

        // We log the call to the server only if we call other endpoint, or loop will occur
        if(path != BuildConfig.LOG_PATH_URL) {
            val tookMs = TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - startNs)
            if(result.isSuccessful) {
                sendLoadLogEvent(tookMs)
            } else
                sendErrorLogEvent(tookMs)
        }

        return chain.proceed(request)
    }

    private fun sendLoadLogEvent(requestTimeInMillis: Long){
        FruitLogger(Repository(), AppSchedulerProvider()).logEvent(LogType.LOAD, requestTimeInMillis.toString())

    }

    private fun sendErrorLogEvent(requestTimeInMillis: Long){
        FruitLogger(Repository(), AppSchedulerProvider()).logEvent(LogType.ERROR, requestTimeInMillis.toString())
    }
}