package com.lpirro.fruit.di

import com.lpirro.fruit.app.application.FruitApplication
import com.lpirro.fruit.app.fruitlist.FruitListFragment
import com.lpirro.fruit.app.fruitlist.FruitListModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, FruitListModule::class])
interface ApplicationComponent {
    fun inject(application: FruitApplication)
    fun inject(fragment: FruitListFragment)
}