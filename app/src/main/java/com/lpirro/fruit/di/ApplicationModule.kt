package com.lpirro.fruit.di

import android.app.Application
import android.content.Context
import com.lpirro.fruit.data.Repository
import com.lpirro.fruit.utils.AppSchedulerProvider
import com.lpirro.fruit.utils.SchedulerProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(val application: Application) {

    @Provides
    @Singleton
    fun provideContext(): Context = application

    @Provides
    @Singleton
    fun provideRepository(): Repository {
        return Repository()
    }

    @Provides
    fun provideSchedulerProvider(): SchedulerProvider {
        return AppSchedulerProvider()
    }
}