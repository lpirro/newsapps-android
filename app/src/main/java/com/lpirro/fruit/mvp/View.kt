package com.lpirro.fruit.mvp

interface View {
    fun showLoading(active: Boolean)

    fun showError(error: Throwable)
}
