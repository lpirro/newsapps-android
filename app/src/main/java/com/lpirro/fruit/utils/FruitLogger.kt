package com.lpirro.fruit.utils

import com.lpirro.fruit.data.Repository
import com.lpirro.fruit.data.models.LogType

class FruitLogger(private val repository: Repository, private val schedulerProvider: SchedulerProvider) {

    fun logEvent(logType: LogType, logText: String, callback: LoggerCallback? = null) {

        val disposable = repository.logEvent(logType, logText)
                .subscribeOn(schedulerProvider.ioScheduler())
                .observeOn(schedulerProvider.uiScheduler())
                .subscribe(
                        { callback?.onLogPerformed() },
                        { callback?.onLogPerformed() }
                )
    }
}

interface LoggerCallback {
    fun onLogPerformed()
}