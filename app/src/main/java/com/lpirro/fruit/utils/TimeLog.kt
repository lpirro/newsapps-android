package com.lpirro.fruit.utils

class Timelog {

    private var startTime: Long = System.currentTimeMillis()

    val elapsedTime: Long get() =
        System.currentTimeMillis() - startTime


    fun resetStartTime() {
        startTime = System.currentTimeMillis()
    }
}