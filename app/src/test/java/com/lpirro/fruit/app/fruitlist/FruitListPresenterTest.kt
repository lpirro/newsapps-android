package com.lpirro.fruit.app.fruitlist

import com.lpirro.fruit.app.fruitlist.util.StaticData
import com.lpirro.fruit.data.Repository
import com.lpirro.fruit.data.models.FruitApiResponse
import com.lpirro.fruit.data.network.api.ApiService
import com.lpirro.fruit.utils.TestSchedulerProvider
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class FruitListPresenterTest {

    private lateinit var presenter: FruitListPresenter
    private var view = mock<FruitListContract.FruitsView>()
    private val service = mock<ApiService>()
    private var repo = mock<Repository>()

    @Before
    fun setup(){
        repo.apiService = service
        presenter = FruitListPresenter(repo, TestSchedulerProvider())
        presenter.attachView(view)
    }

    @Test
    fun `get fruit should show fruit list`() {
        val fruits = StaticData.generateFruitList()
        val fakeResponse = FruitApiResponse(fruits)
        val single: Single<FruitApiResponse> = Single.just(fakeResponse)

        doReturn(single).`when`(repo.apiService).getFruit()

        presenter.getFruits()

        verify(view).showFruits(fruits)
    }

    @Test
    fun `get fruit error should show error message`() {

        doReturn(Single.error<Any>(Exception("dummy exception"))).`when`(repo.apiService).getFruit()

        presenter.getFruits()
        verify(view).showError(any())
    }

    @Test
    fun `get fruits should show loading`() {
        val fruits = StaticData.generateFruitList()
        val fakeResponse = FruitApiResponse(fruits)
        val single: Single<FruitApiResponse> = Single.just(fakeResponse)

        doReturn(single).`when`(repo.apiService).getFruit()


        val inOrder = Mockito.inOrder(view)
        presenter.getFruits()

        inOrder.verify(view).showLoading(true)
        inOrder.verify(view).showLoading(false)

    }
}