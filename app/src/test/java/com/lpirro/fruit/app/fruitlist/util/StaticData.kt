package com.lpirro.fruit.app.fruitlist.util

import com.lpirro.fruit.data.models.Fruit

object StaticData {
    fun generateFruitList(): List<Fruit> {
        val fruitList = ArrayList<Fruit>()

        fruitList.add(Fruit("Apple", 100.0, 300.0))
        fruitList.add(Fruit("Banana", 200.0, 200.0))
        fruitList.add(Fruit("Kiwi", 300.0, 100.0))

        return fruitList
    }
}
